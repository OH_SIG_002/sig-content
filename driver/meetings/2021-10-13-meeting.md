#  October 13, 2021 at 16:00am-18:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                             | 发言人                                                       |
  | :---------: | -------------------------------- | ------------------------------------------------------------ |
  | 16:00-16:15 | 议题一、Media的codec驱动能力对齐 | crescenthe，vb6174，starfish，magekkkk，zhangguorong，zianed |
  | 16:10-16:40 | 议题二、Media的codecHDI接口评审  | crescenthe，vb6174，starfish，magekkkk，zhangguorong，zianed |
  | 16:40-17:00 | 议题三、USB HDI接口定义评审      | YUJIA，xzmu，zianed                                          |

## Attendees

- @[crescenthe](https://gitee.com/crescenthe)
- @[zianed](https://gitee.com/zianed)
- @[YUJIA](https://gitee.com/JasonYuJia)
- @[Kevin-Lau](https://gitee.com/Kevin-Lau)
- @[vb6174](https://gitee.com/vb6174)
- @[magekkkk](https://gitee.com/magekkkk)
- @xzmu(kelei@iscas.ac.cn)
- @ starfish(https://gitee.com/starfish002)
- @bayanxing(bayanxing050@chinasoftinc.com)
- @zhangguorong(zhangguorong050@chinasoftinc.com)

## Notes

- **议题一、Media的codec驱动能力开发进展**

   结论：基于HDF驱动模型，已启动codec编码，完成代码框架搭建部分实现。其中项目整体交付进度有风险，crescenthe投入部分时间，参与开发，确保交付进度。另外需要补充框架详细设计文档。
   
- **议题二：Media的codec HDI接口修改评审**
   结论：1、codec HDI接口变更，其中2个接口变更，对应的部分结构体变更，会议评审相关接口和结构体变更合理，评审通过。

   ​           2、变更接口pr链接：https://gitee.com/openharmony/drivers_peripheral/pulls/323 
   
   遗留问题： 1、 CodecCreate 可选参数描述修改； 
   
   2、 宏定义，增加注释； 
   
   3、 supportLevels再明确一下是否是音视频公共项；
   
   4、 接口提交时提供ReleaseNotes，呈现变化点及对应使用说明；
   
   5、 接口使用文档；缺少使用指南（图文呈现各个接口的使用）； 
   
   6、 接口正式更新时间：10.25号， PR先提交（10.16号前）； 
   
- **议题三：USB HDI接口定义评审**
  结论：usb hdi新增25接口定义，经前两次会议评审，遗留问题已闭环，接口评审通过，HDI接口，在10.30日前提交上库。

  
  
  会议通知：https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/5M57EYNLGYSUCPXHZD5NXYRU7CS2KVQQ/
  
  会议议题申报：https://docs.qq.com/sheet/DZnpsUFZIT3BVcWJ5?tab=BB08J2&_t=1632723145103 
  
  会议归档：https://gitee.com/openharmony-sig/sig-content/tree/master/driver/meetings


  ## Action items

  

