#  September 15, 2021 at 16:00am-17:00am GMT+8

## Agenda

- OH Driver SIG例会[](https://meeting.tencent.com/s/O6GmTrcZcV8a)

  |    时间     | 议题                                                         | 发言人                           |
  | :---------: | ------------------------------------------------------------ | -------------------------------- |
  | 16:00-16:10 | 议题一、Media的codec驱动能力进展对齐                         | crescenthe，vb6174，zhangguorong |
  | 16:10-16:35 | 议题二、 M核HCS解析成源文件，期望能够提供访问源文件的接口规范讨论 | SimonLi, yuanbo                  |
  | 16:35-17:20 | 议题三、USB HDI接口定义评审                                  | chenfeng469,yuchao               |
  | 17:20-17:40 | 议题四、外设传感器支持ppg器件讨论                            | Kevin-Lau,Muffin                 |

## Attendees

- @[chenfeng469](https://gitee.com/chenfeng469)
- @[zianed](https://gitee.com/zianed)
- @[dxbedu](https://gitee.com/dxbedu)
- @[Kevin-Lau](https://gitee.com/Kevin-Lau)
- @[yuanbo](https://gitee.com/yuanbogit)
- @[SimonLi](https://gitee.com/kkup180)
- @[Muffin](kelei@iscas.ac.cn)
- @[vb6174](https://gitee.com/vb6174)
- @[magekkkk](https://gitee.com/magekkkk)
- @minglong(minglong@iscas.ac.cn)
- @bayanxing(bayanxing050@chinasoftinc.com)
- @zhangguorong(zhangguorong050@chinasoftinc.com)

## Notes

- **议题一、Media的codec驱动能力开发进展**

   结论：根据前期需求澄清和分析，已和soc厂商进行硬件编解码驱动方案进行讨论，已细化开发交付计划，按照计划开发和交付。
   遗留问题：1.需要细化验收标准及自测试方案。责任人：crescenthe,zhangguorong
      2.在孵化项目上新建drivers仓，完成时间9月30日，责任人 zianed。 
   
- **议题二：M核HCS解析成源文件，期望能够提供访问源文件的接口规范讨论**
   结论：接收提供M核HCS解析源文件接口使用规范建议，有yuanbo会后整理相关规范文档，在10月份sig例会上申报议题，审视进展。

- **议题三：USB HDI接口定义评审**
  结论：HDI接口评议不通过，需要按照讨论建议整改，下次sig例会重新评审。

  讨论建议如下:1.HDI接口函数定义合理性；2.接口入参个数符合规范；3.接口入参为指针，需要考虑内存所有权问题，接口生命周期（同步，异步）；4.梳理接口逻辑关系，接口使用者能清晰接口功能使用。责任人：yuchao

- **议题四：外设传感器支持ppg器件讨论**
  会议讨论当前开源板硬件环境是否满足ppg传感器外接问题，开源版需要外接ppg传感器，硬件诉求为1路spi，2个gpio，2路电，从预留接口看满足，具体硬件方案会后和开源3516板硬件工程细化讨论。ppg驱动开发后，代码贡献需要符合openharmony编程规范。对于ppg驱动设计方案，其中包括设备上下电管理，spi协议等方案明确后，希望能明确交付计划，参与传感器器件开源社区贡献。

-  详细会议纪要：https://shimo.im/sheets/36GKhpvrXd8TcQHY/MODOC

  openharmony编程规范：https://gitee.com/openharmony/docs/blob/master/zh-cn/contribute/%E8%B4%A1%E7%8C%AE%E4%BB%A3%E7%A0%81.md


  ## Action items

  

