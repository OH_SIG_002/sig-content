# Apr.12, 2022 at 10:00am GMT+8

## Agenda
|时间|议题|发言人|
|--|--|--|
|10:00-10:15|沟通芯联芯适配clang编译工具的问题|Lain，黄慧进，陈牡丹|
|10:15-10:25|同步x2000合并主干部分适配进展|洪涛 袁祥仁 刘佳科|
|10:25-10:30|沟通预估x2000子任务完成时间|洪涛 袁祥仁 黄首西|

## Attendees
- [@hongtao6573](https://gitee.com/hongtao6573)
- @Lain
- [@wicom](https://gitee.com/wicom)
- [@xiongtao](https://gitee.com/xiongtao)
- @黄慧进
- @陈国栋
- @陈牡丹
- [@libing23](https://gitee.com/libing23)
- @王栋
- [@huangsox](https://gitee.com/huangsox)
- [@liujk000](https://gitee.com/liujk000)

## Notes

#### 议题一、沟通芯联芯适配clang编译工具的问题
**详情**
- 问题1：编译工具为clang，尝试使用prebuilt内核时，失败。
- 问题2：强制绕开内核编译，使用预编译的sysroot时编译失败。

**结论**

- 以上两个问题确认为clang适配流程问题，需要查询相关文档后再沟通，责任人：刘佳科，Lain。


#### 议题二、同步x2000合并主干部分适配进展

**结论**

- graphic模块，DFX子系统模块，AI模块功能增强都在开发中，目前无阻塞性问题。


#### 议题三、沟通预估x2000子任务完成时间

  **结论**
- 会后根据开发实际情况，发出子任务计划完成时间表。


## Action items
