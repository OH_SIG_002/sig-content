
## C-SDK

### sdk初始化

配置sdk的底层依赖

#### 函数定义

```c
void tiot_sysdep_set_portfile(tiot_sysdep_portfile_t* portfile);
```

#### 参数说明

| 参数类型                | 参数名称 | 参数说明                         |
| ----------------------- | -------- | -------------------------------- |
| tiot_sysdep_portfile_t* | portfile | 结构体类型, 包含底层依赖函数指针 |

`tiot_sysdep_portfile_t`结构体函数指针定义:

| 函数指针名                    | 参数                                                         | 返回值   | 说明                                           |
| ----------------------------- | ------------------------------------------------------------ | -------- | ---------------------------------------------- |
| service_sysdep_malloc            | (uint32_t size, char *name)                                  | void *   | 申请内存                                       |
| service_sysdep_free              | (void *ptr)                                                  | void     | 释放内存                                       |
| service_sysdep_time              | (void)                                                       | uint64_t | 获取当前的时间戳，SDK用于差值计算              |
| service_sysdep_sleep             | (uint64_t time_ms)                                           | void     | 睡眠指定的毫秒数                               |
| service_sysdep_network_init      | (void)                                                       | void     | 创建1个网络会话(L3层)                          |
| service_sysdep_network_setopt    | (void *handle, service_sysdep_network_option_t option, void *data) | int32_t  | 配置1个网络会话的连接参数                      |
| service_sysdep_network_establish | (void *handle)                                               | int32_t  | 建立1个网络会话, 作为MQTT/HTTP等协议的底层承载 |
| service_sysdep_network_recv      | (void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms, service_sysdep_addr_t *addr) | int32_t  | 从指定的网络会话上读取                         |
| service_sysdep_network_send      | (void *handle, uint8_t *buffer, uint32_t len, uint32_t timeout_ms, service_sysdep_addr_t *addr) | int32_t  | 在指定的网络会话上发送                         |
| service_sysdep_network_deinit    | (void **handle)                                              | int32_t  | 销毁1个网络会话                                |
| service_sysdep_rand              | (uint8_t *output, uint32_t output_len)                       | void     | 随机数生成方法                                 |
| service_sysdep_mutex_init        | (void)                                                       | void     | 创建互斥锁                                     |
| service_sysdep_mutex_lock        | (void *mutex)                                                | void     | 申请互斥锁                                     |
| service_sysdep_mutex_unlock      | (void *mutex)                                                | void     | 释放互斥锁                                     |
| service_sysdep_mutex_deinit      | (void **mutex)                                               | void     | 销毁互斥锁                                     |

#### 返回

无

### 回收sdk

释放sdk内存等扫尾工作

#### 函数定义

```c
int32_t tiot_mqtt_deinit(void **handle)
```

#### 参数说明

| 参数类型 | 参数名称 | 参数说明                                 |
| -------- | -------- | ---------------------------------------- |
| void **  | handle   | 实际需要获取service_mqtt_handle_t *释放内存 |

#### 返回值

| 错误码                    | 值      | 说明 |
| ------------------------- | ------- | ---- |
| RET_MQTT_DEINIT_TIMEOUT | -0x0312 | 超时 |
| RET_SUCCESS             | 0x0000  | 成功 |



### sdk log输出

配置sdk log输出回调

#### 函数定义

```c
void tiot_RET_set_logcb(tiot_RET_logcb_t handler);
```

#### 参数说明

| 参数类型           | 参数名称 | 参数说明       |
| ------------------ | -------- | -------------- |
| tiot_RET_logcb_t | handler  | 指向打印的接口 |

#### 返回

无

### 设备注册信息

#### 函数定义

注册设备信息

```cpp
int32_t tiot_mqtt_setopt(void *handle, tiot_mqtt_option_t option, void *data)；
```

#### 参数说明

| 参数类型           | 参数名称 | 参数说明                                 |
| ------------------ | -------- | ---------------------------------------- |
| void *             | handle   | 参数实际类型是service_mqtt_handle_t *的句柄 |
| tiot_mqtt_option_t | option   | 枚举类型，注册设备基础信息               |
| void *             | data     | 根据枚举传参，设置相应功能               |

#### 返回

连接成功与否。成功返回0，失败返回对应的错误码。常见错误码如下:

| 错误码                          | 值      | 说明                                  |
| ------------------------------- | ------- | ------------------------------------- |
| RET_USER_INPUT_NULL_POINTER   | -0x0101 | 用户输入参数中包含非法的空指针        |
| RET_USER_INPUT_OUT_RANGE      | -0x0102 | 用户输入参数中包含越界的值            |
| RET_USER_INPUT_EXEC_DISABLED  | -0x0109 | 用户已调用销毁函数销毁了实例          |
| RET_USER_INPUT_UNKNOWN_OPTION | -0x0103 | 用户输入的配置项不能被SDK的模块所理解 |
| 其他                            |         | 详细文档设计                          |



### 设备认证建连(一机一密)

#### 函数定义

利用已经获取到的三元组信息去进行`mqtt`的连接，并返回相关信息。

```cpp
int tiot_mqtt_connect(void *handle);
```

#### 参数说明

| 参数类型 | 参数名称 | 参数说明                                 |
| -------- | -------- | ---------------------------------------- |
| void *   | handle   | 参数实际类型是service_mqtt_handle_t *的句柄 |

#### 返回

连接成功与否。成功返回0，失败返回对应的错误码。常见错误码如下:

| 错误码                                                       | 值        | 说明                                                         |
| :----------------------------------------------------------- | :-------- | :----------------------------------------------------------- |
| [RET_MQTT_CONNACK_RCODE_SERVER_UNAVAILABLE](http://gaic.alicdn.com/ztms/linkkit/html/aiot__RET__api_8h.html#a9b91df476086fe57733751b4bd0846b4) | `-0x0303` | MQTT服务器拒绝提供连接， 服务当前不可用。                    |
| [RET_MQTT_CONNACK_RCODE_BAD_USERNAME_PASSWORD](http://gaic.alicdn.com/ztms/linkkit/html/aiot__RET__api_8h.html#aca297f1342d9e8411a649296a3929d83) | `-0x0304` | 连接时的用户名或密码非法。                                   |
| [RET_MQTT_CONNACK_RCODE_NOT_AUTHORIZED](http://gaic.alicdn.com/ztms/linkkit/html/aiot__RET__api_8h.html#a9e8c02ab8e8ed5c91a61bb94d913bc5d) | `-0x0305` | MQTT服务器进行连接身份验证失败，登录密码错误。请检查设备认证信息是否正确。 |

### 订阅消息

MQTT订阅Topic功能

#### 函数定义说明

```c
int32_t tiot_mqtt_sub(void *handle, char *topic, tiot_mqtt_recv_handler_t handler, uint8_t qos, void *userdata);
```

#### 参数说明

| 参数类型                 | 参数名称 | 参数说明                                         |
| ------------------------ | -------- | ------------------------------------------------ |
| void *                   | handle   | 操作句柄，存储了主机号、端口号、MQ账号密码等信息 |
| char *                   | topic    | 设备监听的topic主题                              |
| tiot_mqtt_recv_handler_t | handler  | mqtt接收的回调函数                               |
| uint8_t                  | qos      | mqtt消息级别                                     |
| void *                   | userdata | mqtt接收的消息                                   |

#### 返回

| 函数    | 返回值                             |
| ------- | ---------------------------------- |
| int32_t | 成功则返回0,不成功返回对应的错误码 |

### 设备指令回调

#### 函数定义（回调）

对云IoT下发的指令进行接收，此回调函数在tiot_mqtt_setopt中进行注册

```c
void (CallbackExecute*)(void *handle, const tiot_mqtt_recv_t *packet, void *userdata);//回调函数
```

#### 参数说明

| 参数类型                 | 参数名称 | 参数说明                                                     |
| ------------------------ | -------- | ------------------------------------------------------------ |
| void *                   | handle   | 操作句柄，存储了主机号。端口号。MQ账号密码等信息,需要在调用之前将这些值进行设置 |
| const tiot_mqtt_recv_t * | packet   | 接收来自云IoT的包消息,其中包含了请求数据                     |
| void *                   | userdata | 模组所需要传进的参数。如有多个参数可用结构体进行封装         |

#### 返回

### 属性上报数据

### 同接口：订阅消息



### 设备事件上报

设备事件是指设备产生的事件，比如灯开、关事件、温度过高报警事件等。函数实现一个功能, 将设备产生的事件上发给云端IOT

#### 函数定义说明

```c
int32_t tiot_mqtt_pub_with_method(void *handle, char *topic, uint8_t *payload, uint32_t payload_len, uint8_t qos, const uint8_t *pub_method);
```

#### 参数说明

| 参数类型 | 参数名称    | 参数说明                                         |
| -------- | ----------- | ------------------------------------------------ |
| void *   | handle      | 操作句柄，存储了主机号、端口号、MQ账号密码等信息 |
| char*    | topic       | 设备监听的topic主题                              |
| char*    | payload     | 物模型的data数据                                 |
| uint32_t | payload_len | data数据长度                                     |
| uint8_t  | qos         | QoS等级                                          |
| uint8_t  | pub_method  | 上报设备类型                                     |

pub_method格式如下

```
{
    "Power": {
      "value": "on"
    },
    "WF": {
      "value": 23.6,
      "t": 1524448722000
    }
```

#### 返回

| 函数    | 返回值                             |
| ------- | ---------------------------------- |
| int32_t | 成功则返回0,不成功返回对应的错误码 |











