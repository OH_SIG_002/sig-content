#  Nov 4, 2021 at 14:00pm-15:30pm GMT+8

## Agenda

OH Dev-Board SIG例会[会议录制链接](https://pan.baidu.com/s/1nBirtmYXlb1Q-l5lPn8wgQ)（提取码：c7q0）



| **时间**    | **议题**                            | **发言人**                                                   |
| ----------- | ----------------------------------- | ------------------------------------------------------------ |
| 14:00-14:30 | 各单位OH开发板适配进展              | 朗国-颜专、博流-韩锐、博通-王学军、润和-石磊、拓维信息-张健滢、宸松-董昱、黑胡桃-江展翔、小熊派-王城、全志-刘明缘、君正-邱东升、九联-钟义秀 |
| 14:30-14:50 | PMC指派给OH Dev-Board SIG的任务讨论 | 黎高鹏                                                       |
| 14:50-15:10 | L0代码整改讲解                      | 李开龙                                                       |
| 15:10-15:30 | 开发板准入标准（官网）讨论          | 刘洋、黄明龙                                                 |

## Attendees

- [@liuyang198591](liu_yang@hoperun.com)
- [@Laowang-BearPi](wangcheng@holdiot.com)
- [@zhao_xiuxiu ](1605427044@qq.com)
- [@kevenNO1 ](likai20@iscas.ac.cn)
- [@jony_code ](longjun@iscas.ac.cn)
- [@minglonghuang ](minglong@iscas.ac.cn)

## Notes

- 议题一、各单位开发板适配进展与兼容性认证进展详情
  各单位开发板适配进展与兼容性认证进展详见腾讯文档[Dev-Board SIG开发板列表](https://docs.qq.com/sheet/DYmZ1RmhEZ1RVa0to?tab=BB08J2)
  
  开发板的下一个发布节点为11月30日，希望能有更多的开发板加入进来，目前L0开发板：由于已经出了3.0版本，希望开发人力充足的单位可以将开发板升级到此版本，华为会提供相关的技术支持来配合大家完成升级;L1正在解决多媒体的解耦与能力补齐;L2（RK3568）也正在合入主干。经兼容性测试工作组、基金会、华为多方讨论，兼容性会有业务上的刷新，有关PCS和XTS上会有更新，预计本月底大家可以看到兼容性相关的条款和规范，PCS会拉通开发板OEM厂家收集相关诉求，有认为不合理的可以提出来给PMC和TMC进行审核，相关诉求可以发给李凯，邮箱likai20@iscas.ac.cn
  
- 议题二、PMC指派给OH Dev-Board SIG的任务讨论

  1、开发板发布希望PMC给出标准（按照之前刘洋总起草和整理的一个发布标准，计划在下次的PMC会议上作为一个议题讨论，等PMC会议上决策）

  2、业界目前对代码质量、解耦问题，HDF架构等问题还有一些质疑的声音，希望各开发板厂家，芯片伙伴反馈给SIG，来推动内部改进，不断优化。

- 议题三：L0代码整改讲解
  
  L0代码整改相关问题后续在zulip进行讨论
  
- 议题四： 开发板准入标准（官网）讨论

  开发板不是一个标准的产品，不能按照PCS的标准来套用标准，根据功能方面来罗列轻量系统、小型系统、标准系统的一些特性支持要求，线上有单位提出希望加上HDF要求与BLE要求。具体文档请查阅[OpenHarmony开发板准入标准 ](https://gitee.com/openharmony-sig/sig-content/blob/master/devboard/docs/OpenHarmony%E5%BC%80%E5%8F%91%E6%9D%BF%E5%87%86%E5%85%A5.xlsx )

  ## Action items

  1、开发板准入标准新增上HDF要求与BLE要求

  2、适配芯片的型号比较老旧，内核用的是3.1X的内核，目前的Open Harmony支持的是4.19、5.10，后续的对于3.1X的内核是如何考量的？

  